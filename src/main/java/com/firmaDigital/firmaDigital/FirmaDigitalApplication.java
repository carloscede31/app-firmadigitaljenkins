package com.firmaDigital.firmaDigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirmaDigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirmaDigitalApplication.class, args);
	}

}
